from django.contrib import admin
from snippets.models import Snippet, SnippetParticipant

class SnippetAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'created')
    list_display_links = ('id', 'title')
    search_fields = ('title',)
    list_filter = ('created',)
    ordering = ('-created',)


class SnippetParticipantAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', "snippet")
    list_display_links = ('id', 'user', "snippet")
    search_fields = ('user', "snippet")
    list_filter = ('id',)
    ordering = ('-id',)


admin.site.register(Snippet, SnippetAdmin)
admin.site.register(SnippetParticipant, SnippetParticipantAdmin)
