from rest_framework import serializers
from snippets.models import Snippet, SnippetParticipant
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username']


class SnippetSerializer(serializers.ModelSerializer):



    def get_allowed_users(self, obj) -> list[str]:
        query = SnippetParticipant.objects.filter(snippet=obj)
        return [ele.username for ele in query]

    class Meta:
        model = Snippet
        fields = [
            "id",
            "created",
            "title",
            "content",
            "linenos",
            "language",
            "style",
            "owner",
            "access",
        ]
        read_only_fields = ["created", "owner"]
        owner = serializers.ReadOnlyField(source='owner.username')
        allowed_users = serializers.SerializerMethodField(read_only=True)

#
# class SnippetParticipantSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = SnippetParticipant
#         fields = [
#             "id",
#             "user",
#             "snippet"
#         ]
#         read_only_fields = ["id"]
