from snippets.models import Snippet, SnippetParticipant
from snippets.serializers import SnippetSerializer, UserSerializer
from rest_framework import generics
from django.contrib.auth.models import User
from rest_framework import permissions
from snippets.permissions import IsAccessedOrDeny


class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class SnippetList(generics.ListCreateAPIView):
    queryset = Snippet.objects.all()
    serializer_class = SnippetSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class SnippetDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Snippet.objects.all()
    serializer_class = SnippetSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsAccessedOrDeny]


class SnippetParticipantList(generics.ListCreateAPIView):
    queryset = SnippetParticipant.objects.all()
    serializer_class = SnippetSerializer


class SnippetParticipantDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = SnippetParticipant.objects.all()
    serializer_class = SnippetSerializer
