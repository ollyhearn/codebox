import React from "react";
import './App.css';
import HomePage from './pages/HomePage'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { AuthProvider } from "./context/AuthContext";
import AuthPage from './pages/AuthPage'
import SnippetPage from './pages/SnippetPage'
import NewSnippet from './pages/NewSnippetPage'
import PublicSnippetsPage from "./pages/PublicSnippetsPage";

function App() {
    return (

        <Router>
            <AuthProvider>
                <Routes>
                    <Route path="/auth" element={<AuthPage />} />
                    <Route path="/" element={<div className="App">
                        <HomePage />
                        </div>} />
                    <Route path="/snippet/:id" element={<div className="App">
                        <SnippetPage />
                        </div>} />
                    <Route path="/new-snippet" element={<div className="App">
                        <NewSnippet />
                        </div>} />
                    <Route path="/public-snippets" element={<div className="App">
                        <PublicSnippetsPage />
                        </div>} />
                </Routes>
            </AuthProvider>
        </Router>

    );
}

export default App;
