import BasicPage from './BasicPage'
import NewSnippet from '../components/NewSnippet'

const NewSnippetPage = () => {
    return (
        <BasicPage style={{margin: "20%"}}>
            <NewSnippet />
        </BasicPage>
    );
}

export default NewSnippetPage;
