import BasicPage from './BasicPage'
import MainCard from '../components/MainCard'

const Page = () => {
    return (
        <BasicPage style={{margin: "20%"}}>
            <MainCard />
        </BasicPage>
    );
}

export default Page;
