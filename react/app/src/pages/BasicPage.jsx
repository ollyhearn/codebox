import Header from '../components/Header'
import { BrowserRouter as Router, Route } from 'react-router-dom';

const BasicPage = (props, style) => {
    return (
        <div style={style}>
            <Header />
            {props.children}
        </div>
    );
}

export default BasicPage;
