import LoginForm from '../components/LoginForm'
import BasicPage from './BasicPage'

const Page = () => {
    return (
        <BasicPage>
            <LoginForm />
        </BasicPage>
    );
}

export default Page;
