import { useContext, useEffect, useState } from 'react';
import SnippetList from '../components/SnippetList';
import AuthContext from '../context/AuthContext';
import BasicPage from './BasicPage'

const PublicSnippetsPage = () => {
    const { authTokens } = useContext(AuthContext);
    const [ snippets, setSnippets ] = useState(null);

    const getSnippets = async () => {
            const response = await fetch(`http://${process.env.REACT_APP_DOMAIN}/api/snippets/`, {
                method: "GET"
            });
            const data = await response.json();

            if (response.status === 200) {
                setSnippets(data)
            }
        };

    useEffect(() => {
        getSnippets();
    }, []);

    return (
        <BasicPage style={{margin: "20%"}}>
            <SnippetList snippets={snippets} />
        </BasicPage>
    );
}

export default PublicSnippetsPage;
