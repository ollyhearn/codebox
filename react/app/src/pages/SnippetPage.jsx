import { useParams } from 'react-router-dom'
import BasicPage from './BasicPage'
import Snippet from '../components/Snippet'

const SnippetPage = () => {
    const { id } = useParams();
    return (
        <BasicPage style={{margin: "20%"}}>
            <Snippet id={id} />
        </BasicPage>
    );
}

export default SnippetPage;
