import React, { useState } from 'react';
import {
    MDBContainer,
    MDBTabs,
    MDBTabsItem,
    MDBTabsLink,
    MDBTabsContent,
    MDBTabsPane,
    MDBBtn,
    MDBIcon,
    MDBInput,
    MDBCheckbox
}
from 'mdb-react-ui-kit';
import 'mdb-react-ui-kit/dist/css/mdb.min.css';
import "@fortawesome/fontawesome-free/css/all.min.css";
import { useContext } from "react";
import AuthContext from "../context/AuthContext";

const LoginForm = () => {

    const [justifyActive, setJustifyActive] = useState('tab1');

    const handleJustifyClick = (value) => {
        if (value === justifyActive) {
            return;
        }

        setJustifyActive(value);
    };

    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [password2, setPassword2] = useState("")

    const { loginUser, registerUser } = useContext(AuthContext);
    const handleLoginSubmit = () => {
        username.length > 0 && loginUser(username, password);
    };
    const handleRegisterSubmit = () => {
        password === password2 && password.length > 0  ?
            username.length > 0 && registerUser(username, password, password2)
            : alert("Passwords empty or do not match!")
    };

    return (
        <MDBContainer className="p-3 my-5 d-flex flex-column w-50">

            <MDBTabs pills justify className='mb-3 d-flex flex-row justify-content-between'>
                <MDBTabsItem>
                    <MDBTabsLink onClick={() => handleJustifyClick('tab1')} active={justifyActive === 'tab1'}>
                        Login
                    </MDBTabsLink>
                </MDBTabsItem>
                <MDBTabsItem>
                    <MDBTabsLink onClick={() => handleJustifyClick('tab2')} active={justifyActive === 'tab2'}>
                        Register
                    </MDBTabsLink>
                </MDBTabsItem>
            </MDBTabs>

            <MDBTabsContent>

                <MDBTabsPane show={justifyActive === 'tab1'}>

                    <MDBInput wrapperClass='mb-4' label='Username' id='form1' type='text' onInput={e => setUsername(e.target.value)}/>
                    <MDBInput wrapperClass='mb-4' label='Password' id='form2' type='password' onInput={e => setPassword(e.target.value)}/>

                    <div className="d-flex justify-content-between mx-4 mb-4">
                        <MDBCheckbox name='flexCheck' value='' id='flexCheckDefault' label='Remember me' />
                    </div>

                    <MDBBtn
                        className="mb-4 w-100"
                        onClick={handleLoginSubmit}
                    >
                        Sign in
                    </MDBBtn>
                    <p className="text-center">Not a member? <a href="#!" onClick={() => handleJustifyClick('tab2')} active={justifyActive === 'tab2'}>Register</a></p>

                </MDBTabsPane>

                <MDBTabsPane show={justifyActive === 'tab2'}>

                    <MDBInput wrapperClass='mb-4' label='Username' id='form1' type='text' onInput={e => setUsername(e.target.value)}/>
                    <MDBInput wrapperClass='mb-4' label='Password' id='form1' type='password' onInput={e => setPassword(e.target.value)}/>
                    <MDBInput wrapperClass='mb-4' label='Repeat password' id='form1' type='password' onInput={e => setPassword2(e.target.value)}/>

                    <MDBBtn className="mb-4 w-100" onClick={handleRegisterSubmit}>Sign up</MDBBtn>
                    <p className="text-center">Already have an account? <a href="#!" onClick={() => handleJustifyClick('tab1')} active={justifyActive === 'tab1'}>Login</a></p>

                </MDBTabsPane>

            </MDBTabsContent>

        </MDBContainer>
    );
}

export default LoginForm;
