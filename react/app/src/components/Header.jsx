import React, { useState, useContext } from 'react';
import {
    MDBContainer,
    MDBNavbar,
    MDBNavbarBrand,
    MDBNavbarToggler,
    MDBIcon,
    MDBNavbarNav,
    MDBNavbarItem,
    MDBNavbarLink,
    MDBBtn,
    MDBDropdown,
    MDBDropdownToggle,
    MDBDropdownMenu,
    MDBDropdownItem,
    MDBCollapse,
} from 'mdb-react-ui-kit';
import 'mdb-react-ui-kit/dist/css/mdb.min.css';
import "@fortawesome/fontawesome-free/css/all.min.css";
import { Link, useLocation } from 'react-router-dom';
import AuthContext from "../context/AuthContext";

export default function App() {
    const location = useLocation();
    const [showBasic, setShowBasic] = useState(false);
    const { user, logoutUser } = useContext(AuthContext);

    const handleLogout = () => {
        confirm("You are about to logout") && logoutUser();
    };

    return (
        <MDBNavbar expand='lg' light bgColor='light'>
            <MDBContainer fluid>
                <Link to="/">
                <MDBNavbarBrand>CodeBox</MDBNavbarBrand>
                </Link>

                <MDBNavbarToggler
                    aria-controls='navbarSupportedContent'
                    aria-expanded='false'
                    aria-label='Toggle navigation'
                    onClick={() => setShowBasic(!showBasic)}
                >
                    <MDBIcon icon='bars' fas />
                </MDBNavbarToggler>

                <MDBCollapse navbar show={showBasic}>
                    <MDBNavbarNav className='mr-auto mb-2 mb-lg-0'>
                        <MDBNavbarItem>
                            <MDBNavbarLink active aria-current='page' href='#'>
                                Home
                            </MDBNavbarLink>
                        </MDBNavbarItem>
                        <MDBNavbarItem>
                            <MDBNavbarLink href='#'>News</MDBNavbarLink>
                        </MDBNavbarItem>

                        <MDBNavbarItem>
                            <MDBDropdown>
                                <MDBDropdownToggle tag='a' className='nav-link' role='button'>
                                    Snippets
                                </MDBDropdownToggle>
                                <MDBDropdownMenu>
                                    <MDBDropdownItem link href='/public-snippets'>Public snippets</MDBDropdownItem>
                                    <MDBDropdownItem link>My snippets</MDBDropdownItem>
                                    <MDBDropdownItem link>Shared to me snippets</MDBDropdownItem>
                                </MDBDropdownMenu>
                            </MDBDropdown>
                        </MDBNavbarItem>

                        <MDBNavbarItem>
                            <MDBNavbarLink disabled={user && false} href='/new-snippet' tabIndex={-1} aria-disabled='true' background='#3b71ca'>
                                New snippet
                            </MDBNavbarLink>
                        </MDBNavbarItem>
                    </MDBNavbarNav>
                    <MDBNavbarNav right>
                        { user && (
                                        <MDBNavbarItem>
                                                <MDBNavbarLink href='#' aria-disabled='true' background='#3b71ca' onClick={handleLogout}>
                                                        {user?.username}
                                                </MDBNavbarLink>
                                        </MDBNavbarItem>
                                        )}
                </MDBNavbarNav>
                </MDBCollapse>
            </MDBContainer>
        </MDBNavbar>
    );
}
