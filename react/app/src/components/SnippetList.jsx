import { MDBBtn, MDBCard, MDBCardBody, MDBCardHeader, MDBCardTitle } from "mdb-react-ui-kit";
import { useNavigate } from "react-router-dom";

const SnippetList = ({ snippets }) => {
    const navigate = useNavigate();

    const ShortenedSnippet = ({ snippet }) => {
        return (
        <MDBCard>
            <MDBCardHeader>{snippet?.owner}</MDBCardHeader>
            <MDBCardBody>
                <MDBCardTitle>{snippet?.title}</MDBCardTitle>
                <MDBBtn onClick={() => navigate(`/snippet/${snippet?.id}`)}>Open</MDBBtn>
            </MDBCardBody>
        </MDBCard>
        )
    };

    const snippetComponents = snippets?.map((snippet) => {
        return <ShortenedSnippet snippet={snippet} key={snippet.id} />;
    });

    return (
        <div>
            {snippetComponents}
        </div>
    )
};

export default SnippetList;
