import React, { useContext } from 'react';
import {
    MDBCard,
    MDBCardBody,
    MDBCardTitle,
    MDBCardText,
    MDBCardImage,
    MDBBtn,
    MDBRipple
} from 'mdb-react-ui-kit';
import 'mdb-react-ui-kit/dist/css/mdb.min.css';
import "@fortawesome/fontawesome-free/css/all.min.css";
import {Link} from 'react-router-dom';
import AuthContext from "../context/AuthContext";

const MainCard = () => {
    const { user } = useContext(AuthContext);
    return (
            <MDBCard>
                <MDBCardBody>
                    <MDBCardTitle>Welcome to CodeBox!</MDBCardTitle>
                    <MDBCardText>
                        CodeBox is a snippet service to let you securely store your code snippets and share them with your friends, colleagues and others! Register and start sharing your code for free!
                    </MDBCardText>
                        { !user && (
                        <Link to='/auth'>Register</Link>
                        )}
                </MDBCardBody>
            </MDBCard>
    );
}



export default MainCard;
